#!/usr/bin/env perl

# The ten seasons of our love
# Colby Goettel, 2022

my $love;

foreach ($of..$these)
{
  my $long = $days = sitting,
                     waiting,
                     praying;
                     hoping;

  please*do*something;
}

RETURN:
  home;

  SUBSTITUTE:
    my_feelings() for you;

  sub my_feelings
  {
    with.joy, not sadness;
    with.peace, not despair;

    reverse my $misery; push @through;
    close this.hole;
    bind (me, you);

    return $to_me;
  }

for (10..seasons)
{
  together;

  pinnacle of joy - of happiness;
  best_of(weeks, moments);

  say you miss me;
  say you care;

  wait;
  please, wait;

  I can wait;
}

# This was my first attempt at Perl poetry. It's fascinating and fun to work in
# such a highly constrained system. Highly recommend it.
# This poem was written on Perl 5.30.3 and edited on 5.34.1.
