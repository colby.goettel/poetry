# poetry
Various poems. Everything here is a work in progress.

This is also an excuse to learn how to typeset poetry (yay). I have a notebook I use for scribbling down thoughts and this is where I polish things off a bit more, though some poems are stream of consciousness and not edited further.
